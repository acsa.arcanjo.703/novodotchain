using RAVEN = Raven.Client.Documents.Session;

namespace DotChain.Api.Models.Wallets;

public class WalletDocumentDbRepository : IWalletRepository
{
    public void Add(Wallet wallet)
    {
        var store = DocumentStoreHolder.Store;
        using var session = store.OpenSession();
        session.Store(wallet, wallet.PublicAddress);
        session.SaveChanges();
    }

    public Wallet? Get(string address)
    {
        var store = DocumentStoreHolder.Store;
        using var session = store.OpenSession();
        return session.Load<Wallet>(address);
    }
}